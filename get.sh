#!/bin/bash

wget \
     --recursive \
     --no-clobber \
     --page-requisites \
     --html-extension \
     --convert-links \
     --restrict-file-names=windows \
     --domains m.liturgie.cz \
     --no-parent \
         http://m.liturgie.cz/misal/kalendar.htm
